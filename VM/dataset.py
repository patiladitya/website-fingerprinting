from scapy import all as scp
import ipaddress
import pandas as pd
import numpy as np
from urllib.parse import urlparse
import os
import click


#                    Configuration                         #
#----------------------------------------------------------#
WEBLIST = "10web.txt"
CRAWLCOUNT = 30
RESULTDIR = 'results/'
LENGTH = 200         # Length of dataset sample
OUTPUT = 'training.csv'
#----------------------------------------------------------#

def getdirections(data, maxlength):
    # The direction of packet can be determined
    # based on the source or the dest IP.
    # If the source IP is local, then the packet
    # is outgoing, and so on
    directions = []
    for packet in data:
        if not packet.haslayer('IP'):
            continue
        
        IPlayer = packet.getlayer("IP")
        # Is it private?
        ip = IPlayer.src
        if ipaddress.ip_address(ip).is_private:
            direction = "1"
        else:
            direction = "-1"
        # Append value to directions
        directions.append(direction)

    # Truncate list if length > maxlength
    # If list is smaller, increase length to maxlength
    if len(directions) > maxlength:
        del directions[maxlength:]
    else:
        shortcome = maxlength - len(directions)
        directions = directions + (["0"] * shortcome)
        
    # return list of directions
    return directions


@click.command()
@click.option('--weblist', default=WEBLIST, 
    help='Path to file containing list of website links to crawl')
@click.option('--crawlcount', default=CRAWLCOUNT, 
    help='Number of crawls per website')
@click.option('--resultdir', default=RESULTDIR, 
    help='Directory to read pcap files from')
@click.option('--length', default=LENGTH, 
    help='Length of data sample')
@click.option('--output', default=OUTPUT, 
    help='Name of output file')
def makedataset(weblist, crawlcount, resultdir, length, output):
    # Create an empty list
    dataset = []

    # read website name from weblist
    with open(weblist, "r") as f:
        websites = f.readlines()
    
    # Loop through every website in the list
    for website in websites:
        # get hostname
        website = website.strip()
        hostname = urlparse(website).hostname
        print(hostname)
        
        # For every website, read every pcap dump
        for counter in range(0, crawlcount):
            print("processing dump #" , counter)
            # read the packets
            pcap_filename = hostname + "_" + str(counter) + '.pcap'
            if not os.path.exists(resultdir + pcap_filename):
                continue
            data = scp.rdpcap(resultdir + pcap_filename)
            # get directions
            directions = getdirections(data, length)
            # Add the list with website name to dataframe
            directions.append(hostname)
            # Append to dataset list
            dataset.append(directions)
    
    # Create and save dataframe to csv
    npdata = np.array(dataset)
    df = pd.DataFrame(npdata)
    df.to_csv(output, index_label="index")
    
    return df

if __name__ == '__main__':
    makedataset()

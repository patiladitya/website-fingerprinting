import requests
import os
import subprocess
import signal
import time
import sys
from urllib.parse import urlparse

# Number of captures per website
count = int(sys.argv[1])

# Create a requests session
session = requests.session()

# read website list
with open('wiki_top', 'r') as f:
	websites = f.readlines()

for website in websites:
	website = website.strip()
	# hostname
	hostname = urlparse(website).hostname
	# set hostname to website, if hostname is empty
	if hostname == None:
	    hostname = website
	
	for capture in range(count):
		# Start capturing packet
		proc = subprocess.Popen(['tcpdump', '-w', 'results/' + hostname + '_' + str(capture) + '.pcap'])
		pid = proc.pid
		time.sleep(2)
		
		# Set user agent
		headers = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
		}
		
		# Send request
		try:
		    session.get(website, headers=headers)
		except requests.exceptions.ConnectionError:
		    pass
		time.sleep(2)
		
		# Stop capture
		os.kill(pid, signal.SIGINT)

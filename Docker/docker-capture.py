import subprocess
import os
import click
from multiprocessing.dummy import Pool as ThreadPool 
import itertools


def calldocker(website, count):
    # Get current working directory
    cwd = os.getcwd()
    
    # Run docker
    subprocess.run(
    ["docker", "run",
      "-v", cwd + ":/netsec", 
      "-w", "/netsec", 
      "--rm", 
      "dfp", 
      "./script.sh", website, str(count)])
    
@click.command()
@click.option('--filepath', 
    help='path to file with website links')
@click.option('--count', 
    help='Number of times to capture')
def paralleldocker(filepath, count):
    # Get the list of websites
    with open(filepath, 'r') as f:
	    websites = f.readlines()
    
    # Number of captures per container execution
    block = 10
    
    # Modify website list as per block size
    websites = websites * int(int(count) / block)
    
    # Use pool of threads to create one docker container per website
    pool = ThreadPool(20) 
    results = pool.starmap(calldocker,
     zip(websites, itertools.repeat(block)))

if __name__ == "__main__":
    paralleldocker()
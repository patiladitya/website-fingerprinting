import requests
import os
import subprocess
import signal
import time
import sys
import click
from urllib.parse import urlparse
import uuid


'''
Captures a single .pcap file for the website
'''
def capture(session, website, hostname):
    # Get a random uuid to append to filename
    fileid = uuid.uuid4().hex
    
    # Start capturing packet
    proc = subprocess.Popen(['tcpdump', '-w', 'results/' + hostname + '_' + fileid + '.pcap'])
    pid = proc.pid
    time.sleep(2)
		
    # Set user agent
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
    }
		
    # Send request
    try:
        session.get(website, headers=headers)
    except requests.exceptions.ConnectionError:
        pass
    time.sleep(2)
		
    # Stop capture
    os.kill(pid, signal.SIGINT)


'''
Capture count number of requests to website
'''
@click.command()
@click.option('--website', 
    help='website link to capture')
@click.option('--count', 
    help='Number of times to capture')
def multicapture(website, count):
    # Create a requests session
    session = requests.session()
  
    website = website.strip() # eg website: https://www.example.com/
    # hostname
    hostname = urlparse(website).hostname   # eg: www.example.com
    # set hostname to website, if hostname is empty
    if hostname == None:
        hostname = website
	
	# Capture count number of times
    for _ in range(int(count)):
        capture(session, website, hostname)

if __name__ == '__main__':
    multicapture()
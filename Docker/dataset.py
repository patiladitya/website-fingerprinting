from scapy import all as scp
import scapy.error as scperr
import ipaddress
import pandas as pd
import numpy as np
from urllib.parse import urlparse
import os
import click
import glob

#                    Configuration                         #
#----------------------------------------------------------#
WEBLIST = "wiki_top"
RESULTDIR = 'results/'
LENGTH = 200         # Length of dataset sample
OUTPUT = 'training.csv'
TORIP = '172.17.0.1'
#----------------------------------------------------------#

def getdirections(data, maxlength, torip):
    # The direction of packet can be determined
    # based on the source or the dest IP.
    # If the source IP is tor ip, then the packet
    # is incoming, or otherwise
    directions = []
    for packet in data:
        if not packet.haslayer('IP'):
            continue
        
        IPlayer = packet.getlayer("IP")
        # Is it tor IP?
        ip = IPlayer.src
        if ip == torip:
            direction = "-1"
        else:
            direction = "1"
        # Append value to directions
        directions.append(direction)

    # Truncate list if length > maxlength
    # If list is smaller, increase length to maxlength
    if len(directions) > maxlength:
        del directions[maxlength:]
    else:
        shortcome = maxlength - len(directions)
        directions = directions + (["0"] * shortcome)
        
    # return list of directions
    return directions


@click.command()
@click.option('--weblist', default=WEBLIST, 
    help='Path to file containing list of website links to crawl')
@click.option('--resultdir', default=RESULTDIR, 
    help='Directory to read pcap files from')
@click.option('--length', default=LENGTH, 
    help='Length of data sample')
@click.option('--output', default=OUTPUT, 
    help='Name of output file')
@click.option('--torip', default=TORIP, 
    help='IP of the tor entry node')
@click.option('--test', default=False,
    help='Test on 100 samples per website in True')
def makedataset(weblist, resultdir, length, output, torip, test):
    # Create an empty list
    dataset = []

    # read website name from weblist
    with open(weblist, "r") as f:
        websites = f.readlines()
    
    # Loop through every website in the list
    for website in websites:
        # get hostname
        website = website.strip()
        hostname = urlparse(website).hostname
        print(hostname)
        
        # Get list of files for this website
        filelist = glob.glob(resultdir + hostname + "_*.pcap")
        
        # Shorten list if test == True
        if test:
            del filelist[20:]
        
        for pcapfile in filelist:
            # read the packets
            try:
                data = scp.rdpcap(pcapfile)
            except scperr.Scapy_Exception:
                continue
            # get directions
            directions = getdirections(data, length, torip)
            # Add the list with website name to dataframe
            directions.append(hostname)
            # Append to dataset list
            dataset.append(directions)
    
    # Create and save dataframe to csv
    npdata = np.array(dataset)
    df = pd.DataFrame(npdata)
    df.to_csv(output, index_label="index")
    
    return df

if __name__ == '__main__':
    makedataset()

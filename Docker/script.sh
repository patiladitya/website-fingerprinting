#!/bin/sh

# Set variables
website=$1
count=$2

# Get Host IP
hostip=$(ip route show | awk '/default/ {print $3}')
echo $hostip

# Run torsocks to access tor on host
torsocks -P 9100 -a $hostip python ./capture.py --website $website --count $count
